Dotes Local Server
==================

A server to connect the dotes manager to the local filesystem:

    dotes-local-server usage:
            dotes-local-server help    print this message
            dotes-local-server PORT    start server on PORT (must be integer)
            dotes-local-server PORT CONFIGPATH

Provides a (hopefully) REST-ful access to the filesystem at
`localhost:PORT`.  Configuration is read from the following locations.  Once
a configuration file is found, the others are **ignored**:

  - the path specified by `CONFIGPATH`, if given
  - the path specified by env var `DOTESRC`, if present
  - `.dotesrc.yml` in the current directory
  - `.dotesrc.yml` in env var `HOME`
  - `.dotesrc.yml` in user's home directory



All requests require a header `uuid` to be set
to the `uuid` in the project configuration markdown file.
URL arguments are:

  * `projectID`: number identifying the project
  * `path`: path to the file or directory of interest

If a request returns a status code between 200 and 399, the body is the
JSON representation of the data described below.  If the request returns
a status code outside that range, the body is the JSON representation
given by:

  - `status`: the number matching the HTTP status code
  - `errorID`: a unique string ID for this error
  - `error`: the error message
  - `details`: more details regarding the error, or `undefined`

All times are given in `2006-01-02T15:04:05UTC` format.

The `stat` variable is file info, an object containing:

  - `name`: filename
  - `ext`: file extension
  - `path`: full path to file
  - `modTime`: modification time
  - `size`: size of content
  - `isDir`: boolean

URL routes, and the returned body JSON data are:

  * `GET`: `/ping`
    - simple ping check; uuid header ignored
    - request body is ignored
    - response body:
      - `time`
  * `GET`: `/project`
    - get project ID from UUID, if project is registered
    - request body is ignored
    - response body:
      - `projectID`: int for project
  * `POST`: `/project`
    - register project by UUID
    - request body:
      - `path`: local fs path to configuration 
    - response body:
      - `projectID`: int for project
  * `DELETE`: `/project/{projectID}`
    - request body is ignored
    - response body is empty object
  * `POST`: `/project/check`
    - register project by UUID
    - request body:
      - `path`: local fs path to configuration 
    - response is empty object
  * `GET`: `/stat/{projectID}/{path}`
    - get file contents
    - request body is ignored
    - response body is a `stat`
  * `GET`: `/file/{projectID}/{path}`
    - get file contents
    - request body is ignored
    - response body:
      - `stat`
      - `content`: bytes content of the file
  * `POST`: `/file/{projectID}/{path}`
    - post file contents
    - request body:
      - `content`: bytes content of the file 
    - response body is empty object
  * `DELETE`: `/file/{projectID}/{path}`
    - delete file
    - request body is ignored
    - response body is empty object
  * `GET`: `/tree/{projectID}/{path}`
    - get dir contents
    - request body is ignored
    - response body:
      - `stat`
      - `children`: array of `stat`
  * `PUT`: `/tree/{projectID}/{path}`
    - mkdir
    - request body ignored
    - response body is empty object
  * `DELETE`: `/tree/{projectID}/{path}`
    - delete dir
    - request body is ignored
    - response body is empty object
