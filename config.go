package main

//go:generate sh -c "gotpl errors.template.go < errors.yml > errors_gen.go"

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"math/big"
	mrand "math/rand"
	"strings"
	"time"
)

type TLSCertificates struct {
	Cert string
	Key  string
}

type HandlerConfig struct {
	Port  int
	Token string
	TLS   TLSCertificates
}

func GenerateHandlerConfig() (hc HandlerConfig, err error) {
	hc.Port = 1414

	// create token
	tokenLen := 32
	var char = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	b := make([]rune, tokenLen)
	for i := range b {
		randInt, err := rand.Int(rand.Reader, big.NewInt(int64(len(char))))
		if err != nil {
			randInt = big.NewInt(int64(mrand.Intn(len(char))))
		}
		b[i] = char[randInt.Int64()]
	}
	hc.Token = string(b)

	// https://golang.org/src/crypto/tls/generate_cert.go
	priv, err := rsa.GenerateKey(rand.Reader, 4092)
	keyUsage := x509.KeyUsageDigitalSignature | x509.KeyUsageKeyEncipherment | x509.KeyUsageCertSign
	serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
	serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)
	if err != nil {
		err = fmt.Errorf("Failed to generate serial number: %v", err)
		return
	}
	template := x509.Certificate{
		SerialNumber: serialNumber,
		Subject: pkix.Name{
			Organization: []string{"Dotes Local Server"},
		},
		NotBefore: time.Now(),
		NotAfter:  time.Now().Add(time.Hour * 24 * 365 * 100),

		KeyUsage:              keyUsage,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
		DNSNames:              []string{"localhost"},
		IsCA:                  true,
	}

	derBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, &priv.PublicKey, priv)
	if err != nil {
		err = fmt.Errorf("Failed to create certificate: %v", err)
		return
	}
	privBytes, err := x509.MarshalPKCS8PrivateKey(priv)

	var certStr strings.Builder
	var keyStr strings.Builder
	err = pem.Encode(&certStr, &pem.Block{Type: "CERTIFICATE", Bytes: derBytes})
	if err == nil {
		pem.Encode(&keyStr, &pem.Block{Type: "PRIVATE KEY", Bytes: privBytes})
	}
	if err != nil {
		err = fmt.Errorf("Failed to encode certificates: %v", err)
		return
	}
	hc.TLS.Cert = certStr.String()
	hc.TLS.Key = keyStr.String()
	return
}
