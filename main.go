package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"github.com/mitchellh/go-homedir"
	"github.com/spf13/afero"
	"gopkg.in/yaml.v3"
)

func check(e error) {
	if e != nil {
		log.Print(e)
		panic(e)
	}
}

type closeFile struct {
	*os.File
	once sync.Once
}

func (c *closeFile) Close() error {
	var err error
	c.once.Do(func() {
		err = c.File.Close()
	})
	return err
}

func main() {
	os.Exit(mainInner())
}
func mainInner() int {
	var err error
	var help bool
	var configpath string
	var generate bool

	// read flags
	switch {
	case len(os.Args) == 2 && os.Args[1] == "help":
		help = true
	case len(os.Args) == 3 && os.Args[1] == "generate":
		generate = true
		configpath = os.Args[2]
	case len(os.Args) == 2:
		configpath = os.Args[1]
	case len(os.Args) == 1:
	default:
		err = fmt.Errorf("Invalid arguments")
	}

	if err != nil || help {
		if !help {
			fmt.Println(err.Error())
		}
		name := filepath.Base(os.Args[0])
		name = strings.TrimRight(name, filepath.Ext(name))
		fmt.Printf(`%s usage:
	%s help                 print this message
	%s                      start server
	%s CONFIGFILE           start server with specific CONFIGFILE
	%s generate CONFIGFILE  generate config at CONFIGFILE
`,
			name,
			name,
			name,
			name,
			name,
		)
		if !help {
			return 1
		}
		return 0
	}

	if generate {
		f, err := os.Create(configpath)
		if err != nil {
			log.Println("Unable to open file for writing:")
			log.Println("  " + configpath)
			log.Println(err)
			return 1
		}
		defer f.Close()

		hc, err := GenerateHandlerConfig()
		if err != nil {
			log.Println("Internal error:")
			panic(err)
		}

		e := yaml.NewEncoder(f)
		e.Encode(&hc)
		return 0
	}

	// load config file
	var dotesrcEnv string = os.Getenv("DOTESRC")
	var dotesrcName string = "./.dotesrc.yml"
	_, curdirErr := os.Stat(dotesrcName)
	switch {
	case configpath != "":
	case dotesrcEnv != "":
		configpath = dotesrcEnv
	case curdirErr == nil:
		configpath = dotesrcName
	default:
		var homepath string
		homepath, err = homedir.Dir()
		if err != nil {
			log.Println("Unable to determine home directory:")
			log.Print(err)
			return 1
		}
		configpath = filepath.Join(homepath, dotesrcName)
	}
	fmt.Printf("Configuration from:\n  %s\n", configpath)

	// read config file
	configF, err := os.Open(configpath)
	if err != nil {
		log.Println("Unable to read config file:")
		log.Println("  " + configpath)
		log.Print(err)
		return 1
	}
	configClose := closeFile{File: configF}
	defer configClose.Close()
	var config HandlerConfig
	configDecoder := yaml.NewDecoder(configF)
	err = configDecoder.Decode(&config)
	configClose.Close()
	if err != nil {
		log.Println("Unable to parse config file:")
		log.Println("  " + configpath)
		log.Print(err)
		return 1
	}

	// create cert and key files in tempdir
	tmpdir, err := ioutil.TempDir("", "dotes_local_")
	if err != nil {
		log.Println("Unable to create temp directory")
		log.Print(err)
		return 1
	}
	defer os.RemoveAll(tmpdir)

	certfile, err := ioutil.TempFile(tmpdir, "*.cert")
	certClose := closeFile{File: certfile}
	check(err)
	defer certClose.Close()
	_, err = certfile.WriteString(config.TLS.Cert)
	check(err)
	err = certClose.Close()
	check(err)
	defer os.Remove(certfile.Name())

	keyfile, err := ioutil.TempFile(tmpdir, "*.pem")
	check(err)
	keyClose := closeFile{File: keyfile}
	defer keyClose.Close()
	_, err = keyfile.WriteString(config.TLS.Key)
	check(err)
	err = keyClose.Close()
	check(err)
	defer os.Remove(keyfile.Name())

	// start server
	address := fmt.Sprintf("%s:%d", "localhost", config.Port)
	fmt.Printf("listening on %s\n", address)

	h := NewHandler(afero.NewOsFs(), config)

	listener, err := net.Listen("tcp", address)
	if err != nil {
		log.Println("Unable to listen on " + address)
		log.Print(err)
		return 1
	}
	var httpWait sync.WaitGroup
	httpClosedChan := make(chan error, 1)
	httpWait.Add(1)
	var startTime time.Time = time.Now()
	go func() {
		defer httpWait.Done()
		startTime = time.Now()
		httpClosedChan <- http.ServeTLS(listener, h, certfile.Name(), keyfile.Name())
	}()

	// interrupt signal
	kill := make(chan os.Signal, 1)
	signal.Notify(kill, os.Interrupt)

	// wait for kill or http error
	select {
	case <-kill:
		err = nil
	case e := <-httpClosedChan:
		err = e
	}

	if err == nil {
		fmt.Println("KILL signal received; shutting down server")
		listener.Close()
	}
	httpWait.Wait()
	fmt.Println("Server closed:")
	fmt.Printf("  uptime    : %s\n", time.Now().Sub(startTime).Truncate(time.Second).String())
	fmt.Printf("  total hits: %d\n", h.TotalHits())
	if err == nil {
		fmt.Println("Exiting.")
	} else {
		log.Println("HTTP server exited with error:")
		log.Printf("  %s\n", err.Error())
		return 1
	}

	return 0
}
