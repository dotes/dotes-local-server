package main

//go:generate sh -c "gotpl errors.template.go < errors.yml > errors_gen.go"

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
	ignore "github.com/sabhiram/go-gitignore"
	"github.com/spf13/afero"
	"gitlab.com/tbhartman/go-matchedfs"
	"gitlab.com/tbhartman/sectionedfs"
	"gopkg.in/yaml.v3"
)

type project struct {
	Fs         afero.Fs
	ID         int
	UUID       uuid.UUID
	ConfigPath string
}

type handler struct {
	rootFs         afero.Fs
	projects       map[int]project
	projectUUIDMap map[uuid.UUID]int
	nextID         int
	totalRequests  int64
}

func (h *handler) getProjectByID(id int) (project, error) {
	p, ok := h.projects[id]
	if !ok {
		return p, errProjectNotFound
	}
	return p, nil
}
func (h *handler) getProjectByUUID(u uuid.UUID) (project, bool) {
	id, ok := h.projectUUIDMap[u]
	if !ok {
		return project{}, false
	}
	return h.projects[id], true
}
func (h *handler) getProjectFromRequest(res http.ResponseWriter, req *http.Request) (p project, ok bool) {
	u, ok := h.getUUIDFromRequest(res, req)
	if !ok {
		return
	}
	id, err := strconv.Atoi(mux.Vars(req)["id"])
	if err != nil {
		errInvalidProjectID.BodyError().WriteResponse(res)
		ok = false
		return
	}
	p, ok = h.projects[id]
	if !ok {
		errProjectNotFound.BodyError().WriteResponse(res)
		return
	}
	if p.UUID != u {
		errUUIDMismatch.BodyError().WriteResponse(res)
		ok = false
		return
	}
	ok = true
	return
}

func (h *handler) getUUIDFromRequest(res http.ResponseWriter, req *http.Request) (uuid.UUID, bool) {
	uStr := req.Header.Get("uuid")
	u, err := uuid.Parse(req.Header.Get("uuid"))
	if uStr == "" {
		errHeaderUUIDMissing.BodyError().WriteResponse(res)
		return u, false
	}
	if err != nil {
		errHeaderUUIDInvalid.BodyError().WriteResponse(res)
		return u, false
	}
	return u, true
}

type projectConfig struct {
	ConfigPath string
	UUID       uuid.UUID
	Paths      map[string]string
	Exclude    []string
	ReadOnly   []string
}

//----------------------------------------------------------------------------//
// Structs for HTTP request and response bodies to be JSON'd

type bodyError struct {
	Status  int    `json:"status"`
	ErrorID string `json:"errorID"`
	Error   string `json:"error"`
	Details string `json:"details, omitempty"`
}

func (b *bodyError) WriteResponse(res http.ResponseWriter) {
	if b.Status >= 200 && b.Status < 400 {
		panic("bodyError.Status shows HTTP OK (200-399)")
	}
	res.WriteHeader(b.Status)
	e := json.NewEncoder(res)
	e.Encode(b)
}

const bodyTimeFormat string = "2006-01-02T15:04:05UTC"

type bodyTime struct {
	time.Time
}

func (b *bodyTime) MarshalJSON() ([]byte, error) {
	return []byte("\"" + b.Time.Format(bodyTimeFormat) + "\""), nil
}
func (b *bodyTime) UnmarshalJSON(in []byte) error {
	t, err := time.Parse(bodyTimeFormat, strings.Trim(string(in), "\""))
	b.Time = t
	return err
}

type bodyStat struct {
	Name    string   `json:"name"`
	Path    string   `json:"path"`
	Ext     string   `json:"ext"`
	ModTime bodyTime `json:"modTime"`
	Size    int64    `json:"size"`
	IsDir   bool     `json:"isDir"`
}

type emptyBody struct{}

type getPingIn = emptyBody
type getPingOut struct {
	Time bodyTime `json:"time"`
}
type getProjectIn = emptyBody
type getProjectOut struct {
	ProjectID int `json:"projectID"`
}
type postProjectIn struct {
	Path string `json:"path"`
}
type postProjectOut = getProjectOut
type deleteProjectIn = emptyBody
type deleteProjectOut = emptyBody
type postProjectCheckIn = postProjectIn
type postProjectCheckOut = emptyBody

type getStatIn = emptyBody
type getStatOut = bodyStat
type getFileIn = emptyBody
type getFileOut struct {
	Stat    bodyStat `json:"stat"`
	Content []byte   `json:"content"`
}
type postFileIn struct {
	Content []byte `json:"content"`
}
type postFileOut = emptyBody
type deleteFileIn = emptyBody
type deleteFileOut = emptyBody
type getTreeIn = emptyBody
type getTreeOut struct {
	Stat     bodyStat   `json:"stat"`
	Children []bodyStat `json:"children"`
}
type putTreeIn = emptyBody
type putTreeOut = emptyBody
type deleteTreeIn = emptyBody
type deleteTreeOut = emptyBody

//----------------------------------------------------------------------------//

func (h *handler) readBody(input interface{}, res http.ResponseWriter, req *http.Request) bool {

	d := json.NewDecoder(req.Body)
	err := d.Decode(input)
	if err != nil {
		var ok bool
		// allow empty input if input is an empty struct
		if io.EOF == err {
			ok = fmt.Sprintf("%T", input) == fmt.Sprintf("%T", &emptyBody{})
		}
		if !ok {
			errInvalidRequestJSON.BodyError().WriteResponse(res)
			return false
		}
	}
	return true
}

func (h *handler) writeBody(input interface{}, status int, res http.ResponseWriter) {
	var out strings.Builder
	e := json.NewEncoder(&out)
	err := e.Encode(input)
	if err != nil {
		errWriteJSONFailed.BodyError().WriteResponse(res)
		return
	}
	res.WriteHeader(status)
	fmt.Fprint(res, out.String())
	return
}

func (h *handler) GetPing(res http.ResponseWriter, req *http.Request) {
	writeCORS(res, req)
	var in getPingIn
	var out getPingOut
	if !h.readBody(&in, res, req) {
		return
	}
	out.Time.Time = time.Now()
	h.writeBody(&out, http.StatusOK, res)
}

func (h *handler) GetProject(res http.ResponseWriter, req *http.Request) {
	writeCORS(res, req)
	var in getProjectIn
	var out getProjectOut
	if !h.readBody(&in, res, req) {
		return
	}
	u, ok := h.getUUIDFromRequest(res, req)
	if !ok {
		return
	}

	id, ok := h.projectUUIDMap[u]
	var pOk bool
	var p project
	if ok {
		p, pOk = h.projects[id]
	}
	if !ok || !pOk {
		errProjectNotFound.BodyError().WriteResponse(res)
		return
	}
	out.ProjectID = p.ID
	h.writeBody(&out, http.StatusOK, res)
}
func (h *handler) PostProjectCheck(res http.ResponseWriter, req *http.Request) {
	writeCORS(res, req)
	var out postProjectCheckOut
	_, ok := h.getConfig(res, req)
	if !ok {
		return
	}
	h.writeBody(&out, http.StatusOK, res)
}

func (h *handler) getConfig(res http.ResponseWriter, req *http.Request) (config projectConfig, ok bool) {
	var in postProjectIn
	if !h.readBody(&in, res, req) {
		return
	}
	reqUUID, ok := h.getUUIDFromRequest(res, req)
	if !ok {
		return
	}
	ok = false

	config.ConfigPath = in.Path
	f, err := h.rootFs.Open(in.Path)
	if err != nil {
		errProjectConfigurationNotFound.BodyError().WriteResponse(res)
		return
	}
	defer f.Close()

	// configBytes, _ := ioutil.ReadAll(f)
	// re := regexp.MustCompile("^---\n([\n.]*?)\n---\n")
	// submatches := re.FindSubmatch(configBytes)
	yd := yaml.NewDecoder(f)
	err = yd.Decode(&config)
	if err != nil {
		errInvalidConfiguration.BodyError().WriteResponse(res)
		return
	}

	if config.UUID != reqUUID {
		errUUIDMismatch.BodyError().WriteResponse(res)
		return
	}
	ok = true
	return
}
func (h *handler) configureProject(config projectConfig) project {
	// project filesystem is
	//  - main os filesystem
	//  - wrapped in a sectionedfs according to config.Paths
	//  - wrapped in a matchedfs according to excludes, readOnly
	sectioned := sectionedfs.New()
	matched := matchedfs.New(sectioned)

	if len(config.Exclude) > 0 {
		excludes := ignore.CompileIgnoreLines(config.Exclude...)
		matched.SetExcludePathMatcher(excludes)
	}
	if len(config.ReadOnly) > 0 {
		readOnly := ignore.CompileIgnoreLines(config.ReadOnly...)
		matched.AddPermissionPathMatcher(readOnly, 0222)
	}

	for k, v := range config.Paths {
		if !filepath.IsAbs(v) {
			v = filepath.Join(filepath.Dir(config.ConfigPath), v)
		}
		sectioned.RegisterSection(k, afero.NewBasePathFs(h.rootFs, v))
	}

	return project{
		Fs:         matched,
		UUID:       config.UUID,
		ConfigPath: config.ConfigPath,
	}
}
func (h *handler) PostProject(res http.ResponseWriter, req *http.Request) {
	writeCORS(res, req)
	var out postProjectOut
	config, ok := h.getConfig(res, req)
	if !ok {
		return
	}
	u, ok := h.getUUIDFromRequest(res, req)
	if !ok {
		return
	}
	p, ok := h.projects[h.projectUUIDMap[u]]
	if ok {
		out.ProjectID = p.ID
		h.writeBody(&out, http.StatusOK, res)
		return
	}

	p = h.configureProject(config)
	p.ID = h.nextID
	h.nextID++
	h.projects[p.ID] = p
	h.projectUUIDMap[config.UUID] = p.ID

	out.ProjectID = p.ID
	h.writeBody(&out, http.StatusCreated, res)
}
func (h *handler) RepostProject(res http.ResponseWriter, req *http.Request) {
	writeCORS(res, req)
	var out postProjectOut
	config, ok := h.getConfig(res, req)
	if !ok {
		return
	}
	p, ok := h.getProjectFromRequest(res, req)
	if !ok {
		return
	}
	oldID := p.ID
	p = h.configureProject(config)
	p.ID = oldID
	h.projects[oldID] = p

	out.ProjectID = p.ID
	h.writeBody(&out, http.StatusOK, res)
}
func (h *handler) DeleteProject(res http.ResponseWriter, req *http.Request) {
	var out deleteProjectOut
	p, ok := h.getProjectFromRequest(res, req)
	if !ok {
		return
	}
	delete(h.projectUUIDMap, p.UUID)
	delete(h.projects, p.ID)
	h.writeBody(&out, http.StatusOK, res)
}

// pathDirStart gets the project and path and writes the appropriate
// header to ResponseWriter if there is an error.  It also writes CORS
func (h *handler) pathDirStart(res http.ResponseWriter, req *http.Request) (p project, path string, ok bool) {
	writeCORS(res, req)
	p, ok = h.getProjectFromRequest(res, req)
	if !ok {
		return
	}

	path, ok = mux.Vars(req)["path"]
	if !ok {
		e := errInternalError.BodyError()
		e.Details = "Unable to parse path from URL"
		e.WriteResponse(res)
		return
	}
	return
}

type FileInfo struct {
	Path    string
	Name    string
	Ext     string
	URL     string
	ModTime bodyTime
	IsDir   bool
}

type TreeGetResponse struct {
	ProjectID int
	URL       string
	Path      string
	Children  []FileInfo
}

func (h *handler) checkPathError(err error, isRead bool, res http.ResponseWriter) bool {
	switch {
	case err == nil:
		return true
	case os.IsNotExist(err):
		errPathNotFound.BodyError().WriteResponse(res)
		return false
	case os.IsPermission(err):
		if isRead {
			errPathReadPermission.BodyError().WriteResponse(res)
		} else {
			errPathWritePermission.BodyError().WriteResponse(res)
		}
		return false
	default:
		e := errInternalError.BodyError()
		e.Details = err.Error()
		e.WriteResponse(res)
		return false
	}
}
func (h *handler) GetDir(res http.ResponseWriter, req *http.Request) {
	var out getTreeOut
	proj, path, ok := h.pathDirStart(res, req)
	if !ok {
		return
	}

	d, err := proj.Fs.Open(path)
	ok = h.checkPathError(err, true, res)
	if !ok {
		return
	}
	defer d.Close()
	infos, err := d.Readdir(0)
	if err != nil {
		e := errInternalError.BodyError()
		e.Details = err.Error()
		e.WriteResponse(res)
		return
	}
	out.Stat.Path = path
	out.Stat.Name = filepath.Base(path)
	for _, i := range infos {
		var ext string
		if !i.IsDir() {
			ext = filepath.Ext(i.Name())
		}
		var t bodyTime = bodyTime{
			Time: i.ModTime(),
		}
		out.Children = append(out.Children, bodyStat{
			Size:    i.Size(),
			Ext:     ext,
			IsDir:   i.IsDir(),
			ModTime: t,
			Name:    i.Name(),
			Path:    path + "/" + i.Name(),
		})
	}
	h.writeBody(&out, http.StatusOK, res)
}
func (h *handler) PutDir(res http.ResponseWriter, req *http.Request) {
	var out putTreeOut
	proj, path, ok := h.pathDirStart(res, req)
	if !ok {
		return
	}
	// TODO: allow to set permission
	err := proj.Fs.Mkdir(path, os.ModeDir|0770)
	ok = h.checkPathError(err, false, res)
	if !ok {
		return
	}
	h.writeBody(&out, http.StatusCreated, res)
}
func (h *handler) DeleteDir(res http.ResponseWriter, req *http.Request) {
	var out putTreeOut
	proj, path, ok := h.pathDirStart(res, req)
	if !ok {
		return
	}
	stat, err := proj.Fs.Stat(path)
	if err == nil && !stat.IsDir() {
		errPathNotADirectory.BodyError().WriteResponse(res)
		return
	}
	err = proj.Fs.Remove(path)
	if err != nil && !os.IsExist(err) {
		ok = h.checkPathError(err, false, res)
		if !ok {
			return
		}
	}
	h.writeBody(&out, http.StatusOK, res)
}
func (h *handler) GetFile(res http.ResponseWriter, req *http.Request) {
	var out getFileOut
	proj, path, ok := h.pathDirStart(res, req)
	if !ok {
		return
	}
	f, err := proj.Fs.Open(path)
	ok = h.checkPathError(err, true, res)
	if !ok {
		return
	}
	defer f.Close()

	stat, err := f.Stat()
	ok = h.checkPathError(err, true, res)
	if !ok {
		return
	}

	b, err := ioutil.ReadAll(f)
	ok = h.checkPathError(err, true, res)
	if !ok {
		return
	}

	out.Content = b
	out.Stat.Ext = filepath.Ext(path)
	out.Stat.Name = filepath.Base(path)
	out.Stat.Path = path
	out.Stat.ModTime.Time = stat.ModTime()
	out.Stat.Size = stat.Size()
	out.Stat.IsDir = stat.IsDir()

	h.writeBody(&out, http.StatusOK, res)
}
func (h *handler) GetStat(res http.ResponseWriter, req *http.Request) {
	var out getStatOut
	proj, path, ok := h.pathDirStart(res, req)
	if !ok {
		return
	}
	s, err := proj.Fs.Stat(path)
	ok = h.checkPathError(err, true, res)
	if !ok {
		return
	}

	out.Ext = filepath.Ext(path)
	out.Name = filepath.Base(path)
	out.Path = path
	out.ModTime.Time = s.ModTime()
	out.Size = s.Size()
	out.IsDir = s.IsDir()

	h.writeBody(&out, http.StatusOK, res)
}
func (h *handler) PostFile(res http.ResponseWriter, req *http.Request) {
	var in postFileIn
	var out postFileOut
	proj, path, ok := h.pathDirStart(res, req)
	if !ok {
		return
	}
	ok = h.readBody(&in, res, req)
	if !ok {
		return
	}

	f, err := proj.Fs.Create(path)
	ok = h.checkPathError(err, false, res)
	if !ok {
		return
	}
	defer f.Close()

	_, err = f.Write(in.Content)
	ok = h.checkPathError(err, false, res)
	if !ok {
		return
	}

	h.writeBody(&out, http.StatusCreated, res)
}
func (h *handler) DeleteFile(res http.ResponseWriter, req *http.Request) {
	var out deleteFileOut
	proj, path, ok := h.pathDirStart(res, req)
	if !ok {
		return
	}

	err := proj.Fs.Remove(path)
	ok = h.checkPathError(err, false, res)
	if !ok {
		return
	}

	h.writeBody(&out, http.StatusOK, res)
}

func writeCORS(res http.ResponseWriter, req *http.Request) {
	// https://stackoverflow.com/a/7605119
	origin := req.Header.Get("Origin")
	if origin != "" {
		thisURL, err := url.Parse(origin)
		if err == nil {
			if thisURL.Hostname() == "localhost" {
				res.Header().Set("Access-Control-Allow-Origin", fmt.Sprintf("%s://%s", thisURL.Scheme, thisURL.Host))
			} else {
				res.Header().Set("Access-Control-Allow-Origin", "https://dotes.gitlab.io")
			}
		}
	}
	res.Header().Set("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS")
	res.Header().Set("Access-Control-Max-Age", " 1000")
	res.Header().Set("Access-Control-Allow-Headers", "Content-Type,uuid,DotesServerToken")
}

type HandlerCounter interface {
	http.Handler
	TotalHits() int64
}

type handlerCounter struct {
	http.Handler
	h *handler
}

func (h *handlerCounter) TotalHits() int64 {
	return h.h.totalRequests
}
func (h *handlerCounter) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	h.Handler.ServeHTTP(res, req)
}

// NewHandler returns a HitHandler with the hit processing started.
func NewHandler(fs afero.Fs, config HandlerConfig) HandlerCounter {
	h := &handler{
		rootFs:         fs,
		projects:       make(map[int]project),
		projectUUIDMap: make(map[uuid.UUID]int),
		nextID:         1,
	}
	// this router handles all requests
	main := mux.NewRouter()
	// this router handles requests after passing through main router
	sub := mux.NewRouter()
	var handleAll http.HandlerFunc = func(res http.ResponseWriter, req *http.Request) {
		h.totalRequests++
		sub.ServeHTTP(res, req)
	}
	main.NotFoundHandler = handleAll

	// serve routes that have the correct token header
	protected := sub.Headers("DotesServerToken", config.Token).Subrouter()

	// support CORS requests
	sub.Methods(http.MethodOptions).HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		writeCORS(res, req)
		res.WriteHeader(http.StatusOK)
		fmt.Fprint(res, "{}")
	})

	protected.Methods(http.MethodGet).Path("/ping").HandlerFunc(h.GetPing)

	// r := m.PathPrefix("/project")
	protected.Methods(http.MethodGet).Path("/project").HandlerFunc(h.GetProject)
	protected.Methods(http.MethodPost).Path("/project").HandlerFunc(h.PostProject)
	protected.Methods(http.MethodPost).Path("/project/check").HandlerFunc(h.PostProjectCheck)
	protected.Methods(http.MethodDelete).Path("/project/{id}").HandlerFunc(h.DeleteProject)
	protected.Methods(http.MethodPost).Path("/project/{id}").HandlerFunc(h.RepostProject)

	tree := protected.Path("/tree/{id}/{path:.*}").Subrouter()
	tree.Methods(http.MethodGet).HandlerFunc(h.GetDir)
	tree.Methods(http.MethodPut).HandlerFunc(h.PutDir)
	tree.Methods(http.MethodDelete).HandlerFunc(h.DeleteDir)

	protected.Methods(http.MethodGet).Path("/stat/{id}/{path:.*}").HandlerFunc(h.GetStat)
	file := protected.Path("/file/{id}/{path:.*}").Subrouter()
	file.Methods(http.MethodGet).HandlerFunc(h.GetFile)
	file.Methods(http.MethodPost).HandlerFunc(h.PostFile)
	file.Methods(http.MethodDelete).HandlerFunc(h.DeleteFile)

	// a route is needed for users to add certificate
	sub.Methods(http.MethodGet).Path("/").HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		res.WriteHeader(http.StatusOK)
		fmt.Fprintln(res, "Connected to server.")
	})

	// all other routes are forbidden
	var notFound http.HandlerFunc = func(res http.ResponseWriter, req *http.Request) {
		res.WriteHeader(http.StatusForbidden)
		fmt.Fprintf(res, "%d\n", http.StatusForbidden)
		fmt.Fprintln(res, "This action is forbidden!")
		fmt.Fprintln(res, "Did you forget a server token in the header?")
	}
	sub.NotFoundHandler = notFound

	return &handlerCounter{
		Handler: main,
		h:       h,
	}
}
