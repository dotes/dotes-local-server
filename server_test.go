package main_test

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"testing"

	ls "gitlab.com/dotes/dotes-local-server"

	"github.com/spf13/afero"
	"github.com/stretchr/testify/assert"
)

const serverToken string = "thisisalongnotrandomservertokentouse"

type tester struct {
	*testing.T
	http.Handler
	afero.Fs
}

func (t *tester) Request(r *http.Request) *http.Response {
	//set the server token for all of these requests
	r.Header.Set("DotesServerToken", serverToken)
	rec := httptest.NewRecorder()
	t.Handler.ServeHTTP(rec, r)
	return rec.Result()
}

func newTester(t *testing.T) *tester {
	fs := afero.NewMemMapFs()
	for path, contents := range map[string]string{
		"a/.dotesrc": `
---
uuid: 0786200e-6845-4839-9c76-b446edb9d5e3
paths:
  main: a
---
`,
		"a/a/test.md": `
---
title: Test
---
Test content
`,
		"a/a/testb.md": `
---
title: TestB
---
Testb content
`,
		"b/.dotesrc": `
---
uuid: d9075013-6b98-41f3-9bb3-d7f5554a740d
paths:
  main: b
exclude:
  - c/
---
`,
		"b/b/test.md": `
---
title: Test
---
Test content
`,
		"b/c/testb.md": `
---
title: TestB
---
Testb content
`,
	} {
		fs.MkdirAll(filepath.Dir(path), 0700)
		f, err := fs.Create(path)
		if err != nil {
			log.Panic(err)
		}
		f.WriteString(strings.TrimSpace(contents))
		f.Close()
	}

	var hc ls.HandlerConfig = ls.HandlerConfig{
		Token: serverToken,
	}
	h := ls.NewHandler(fs, hc)
	return &tester{
		T:       t,
		Fs:      fs,
		Handler: h,
	}
}

type response struct {
	*http.Response
	result interface{}
}

func readBody(r *http.Response, body interface{}) error {
	return json.NewDecoder(r.Body).Decode(body)
}

type errorResult struct {
	Status  int
	ErrorID string
	Error   string
	Details string
}

type registerResult struct {
	Error     errorResult
	ProjectID int
}

func checkError(t *testing.T, res *http.Response, status int, code string) (ok bool) {
	var e errorResult
	err := readBody(res, &e)
	ok = assert.NoError(t, err)
	ok = ok && assert.Equal(t, status, res.StatusCode, e.ErrorID)
	ok = ok && assert.Equal(t, status, e.Status)
	ok = ok && assert.Equal(t, code, e.ErrorID)
	return ok
}
func checkNoError(t *testing.T, res *http.Response, status int) (ok bool) {
	var e errorResult
	var foundErrorValue bool
	assert.GreaterOrEqual(t, res.StatusCode, 200, "expected %d", status)
	assert.Less(t, res.StatusCode, 400, "expected %d", status)
	if res.StatusCode < 200 || res.StatusCode >= 400 {
		err := readBody(res, &e)
		foundErrorValue = assert.NoError(t, err)
	}
	if status != res.StatusCode && foundErrorValue {
		msg := fmt.Sprintf("(%s) %s", e.ErrorID, e.Error)
		if e.Details != "" {
			msg += ": " + e.Details
		}
		ok = assert.Equal(t, status, res.StatusCode, msg)
	} else {
		ok = assert.Equal(t, status, res.StatusCode)
	}
	return
}

func TestNothingRegistered(t *testing.T) {
	h := newTester(t)

	req := httptest.NewRequest("GET", "/project", nil)
	req.Header.Set("uuid", "aab0b987-4820-4561-a342-f3da5cb38a20")
	resp := h.Request(req)

	checkError(t, resp, http.StatusNotFound, "ProjectNotFound")

	req = httptest.NewRequest("DELETE", "/project/1", nil)
	req.Header.Set("uuid", "aab0b987-4820-4561-a342-f3da5cb38a20")
	resp = h.Request(req)

	checkError(t, resp, http.StatusNotFound, "ProjectNotFound")

}

func TestInvalidServerToken(t *testing.T) {
	h := newTester(t)

	req := httptest.NewRequest("GET", "/project", nil)
	req.Header.Set("uuid", "aab0b987-4820-4561-a342-f3da5cb38a20")
	req.Header.Set("DotesServerToken", "a bad token")
	rec := httptest.NewRecorder()
	h.Handler.ServeHTTP(rec, req)

	assert.EqualValues(t, http.StatusForbidden, rec.Result().StatusCode)
}

func TestNoServerToken(t *testing.T) {
	h := newTester(t)

	req := httptest.NewRequest("GET", "/project", nil)
	req.Header.Set("uuid", "aab0b987-4820-4561-a342-f3da5cb38a20")
	rec := httptest.NewRecorder()
	h.Handler.ServeHTTP(rec, req)

	assert.EqualValues(t, http.StatusForbidden, rec.Result().StatusCode)

	// but allow at the root in order to get certificate
	req = httptest.NewRequest("GET", "/", nil)
	rec = httptest.NewRecorder()
	h.Handler.ServeHTTP(rec, req)

	assert.EqualValues(t, http.StatusOK, rec.Result().StatusCode)
}

func TestPing(t *testing.T) {
	h := newTester(t)

	req := httptest.NewRequest("GET", "/ping", nil)
	resp := h.Request(req)
	checkNoError(t, resp, http.StatusOK)
}

func getJSON(in interface{}) io.Reader {
	var b strings.Builder
	e := json.NewEncoder(&b)
	err := e.Encode(in)
	if err != nil {
		panic(err)
	}
	return strings.NewReader(b.String())
}

func TestItAll(t *testing.T) {
	a := assert.New(t)
	h := newTester(t)

	uuidA := "0786200e-6845-4839-9c76-b446edb9d5e3"

	// register a new project
	req := httptest.NewRequest(
		"POST",
		"/project",
		strings.NewReader(fmt.Sprintf(`
{
	"uuid": "%s",
	"path": "a/.dotesrc"
}
		`, uuidA)),
	)
	req.Header.Set("uuid", uuidA)
	resp := h.Request(req)

	a.Equal(http.StatusCreated, resp.StatusCode)
	var result registerResult
	a.NoError(readBody(resp, &result))
	a.EqualValues(1, result.ProjectID)

	// get file contents
	var treeResult ls.TreeGetResponse
	req = httptest.NewRequest("GET", "/tree/1/", nil)
	req.Header.Set("uuid", uuidA)
	resp = h.Request(req)
	a.Equal(http.StatusOK, resp.StatusCode)
	var b strings.Builder
	io.Copy(&b, resp.Body)
	json.NewDecoder(strings.NewReader(b.String())).Decode(&treeResult)
	if a.Equal(1, len(treeResult.Children)) {
		a.Equal("main", treeResult.Children[0].Name)
		a.True(treeResult.Children[0].IsDir)
	}
	a.Equal(
		regexp.MustCompile("[\t\n ]").ReplaceAllString(`
{
	"stat": {
		"name": ".",
		"path": "",
		"ext": "",
		"modTime":"0001-01-01T00:00:00UTC",
		"size": 0,
		"isDir": false
	},
	"children":[
		{
			"name":"main",
			"path":"/main",
			"ext":"",
			"modTime": "0001-01-01T00:00:00UTC",
			"size":0,
			"isDir":true
		}
	]
}
`,
			""),
		strings.TrimSpace(b.String()),
	)

	// get without a uuid
	req = httptest.NewRequest("GET", "/file/1/main/note.md", nil)
	resp = h.Request(req)
	a.Equal(http.StatusForbidden, resp.StatusCode)

	// get non-existent
	req = httptest.NewRequest("GET", "/file/1/main/note.md", nil)
	req.Header.Set("uuid", uuidA)
	resp = h.Request(req)
	a.Equal(http.StatusNotFound, resp.StatusCode)

	// post a file
	req = httptest.NewRequest(
		"POST",
		"/file/1/main/note.md",
		getJSON(struct{ Content []byte }{Content: []byte("i wrote a note")}),
	)
	req.Header.Set("uuid", uuidA)
	resp = h.Request(req)
	checkNoError(t, resp, http.StatusCreated)

	// check that file was created
	stat, err := h.Stat("a/a/note.md")
	if a.NoError(err) {
		a.EqualValues(14, stat.Size())
		a.True(stat.Mode()&0200 > 0, "File is not writable; mode: %s", stat.Mode())
		a.True(stat.Mode()&0400 > 0, "File is not readable; mode: %s", stat.Mode())
	}

	// post to a path I shouldnt
	// note, these test use the MemMapFs, which allows creating in a
	// directory that does not yet exist.
	req = httptest.NewRequest(
		"POST",
		"/file/1/note.md",
		getJSON(struct{ content []byte }{content: []byte("i wrote a note")}),
	)
	req.Header.Set("uuid", uuidA)
	resp = h.Request(req)
	a.Equal(http.StatusForbidden, resp.StatusCode)

	// delete a nonexistent directory
	req = httptest.NewRequest("DELETE", "/tree/1/main/subdir", nil)
	req.Header.Set("uuid", uuidA)
	resp = h.Request(req)
	checkError(t, resp, http.StatusNotFound, "PathNotFound")

	// create a directory
	req = httptest.NewRequest("PUT", "/tree/1/main/subdir", nil)
	req.Header.Set("uuid", uuidA)
	resp = h.Request(req)
	checkNoError(t, resp, http.StatusCreated)

	// check that it exists
	stat, err = h.Stat("a/a/subdir")
	if a.NoError(err) {
		a.True(stat.IsDir())
	}

	// remove a directory
	req = httptest.NewRequest("DELETE", "/tree/1/main/subdir", nil)
	req.Header.Set("uuid", uuidA)
	resp = h.Request(req)
	a.Equal(http.StatusOK, resp.StatusCode)

	// check that it is gone
	_, err = h.Stat("a/a/subdir")
	a.Error(err)
	a.True(os.IsNotExist(err))

	// remove a file I made
	req = httptest.NewRequest("DELETE", "/file/1/main/note.md", nil)
	req.Header.Set("uuid", uuidA)
	req.Header.Set("DotesServerToken", serverToken)
	resp = h.Request(req)
	checkNoError(t, resp, http.StatusOK)

	// check that file was removed
	_, err = h.Stat("a/a/note.md")
	a.Error(err)
	a.True(os.IsNotExist(err))
}

// func TestOptionsFromOtherHost(t *testing.T) {
// 	a := assert.New(t)
// 	s := server{}
// 	s.Init()
// 	defer s.Close()

// 	req := httptest.NewRequest("OPTIONS", "http://test.example.com/hit", nil)
// 	req.Header.Set("Origin", "http://www.example2.com")
// 	resp := s.ServeHTTP(req)

// 	s.Close()
// 	a.Equal(http.StatusForbidden, resp.StatusCode)
// }

// func TestHit(t *testing.T) {
// 	a := assert.New(t)
// 	s := server{}
// 	s.Init()
// 	defer s.Close()

// 	req := httptest.NewRequest(
// 		"POST",
// 		"http://test.example.com/hit",
// 		strings.NewReader(`{"url":"https://www.example.com/page1"}`),
// 	)
// 	req.Header.Set("Origin", "http://www.example.com")
// 	resp := s.ServeHTTP(req)

// 	s.Close()
// 	a.Equal(http.StatusOK, resp.StatusCode)

// 	a.EqualValues(1, s.Store.Meta().Hits)
// }

// func TestAnalyticsResults(t *testing.T) {
// 	a := assert.New(t)
// 	s := server{}
// 	s.Init()
// 	defer s.Close()

// 	// hit several times
// 	for i := 0; i < 3; i++ {
// 		for j := i; j < 3; j++ {
// 			req := httptest.NewRequest(
// 				"POST",
// 				"http://test.example.com/hit",
// 				strings.NewReader(fmt.Sprintf(`{"url":"https://www.example.com/page/%d"}`, i)),
// 			)
// 			req.Header.Set("Origin", "http://www.example.com")
// 			s.ServeHTTP(req)
// 		}
// 	}
// 	s.Close()

// 	req := httptest.NewRequest(
// 		"GET",
// 		fmt.Sprintf("http://test.example.com/%s/list", serverPrivateKey),
// 		nil,
// 	)
// 	resp := s.ServeHTTP(req)

// 	a.Equal(http.StatusOK, resp.StatusCode)

// 	expected := `
// 		  Uptime              : 0s
// 		  Current time        : 2006-01-02 15:04 MST
// 		  Last hit            : 2006-01-02 15:04 MST
// 		  Session server hits :                   10
// 		  Session page hits   :                    7
// 		  Total page hits     :                    6

// 		+--------------------------------+-----------+
// 		|              PAGE              | HIT COUNT |
// 		+--------------------------------+-----------+
// 		| https://www.example.com/page/0 |         3 |
// 		| https://www.example.com/page/1 |         2 |
// 		| https://www.example.com/page/2 |         1 |
// 		+--------------------------------+-----------+`

// 	bodyBytes, _ := ioutil.ReadAll(resp.Body)
// 	body := string(bodyBytes)
// 	re := regexp.MustCompile(": 2[0-9]{3}(-[0-9]{2}){2} [0-9]{2}:[0-9]{2} \\S+")
// 	for _, s := range []*string{&body, &expected} {
// 		*s = re.ReplaceAllStringFunc(
// 			*s,
// 			func(b string) (ret string) {
// 				ret = ": "
// 				for range b[2:] {
// 					ret += "X"
// 				}
// 				return
// 			},
// 		)
// 	}
// 	checkStrings(
// 		t,
// 		"listing response",
// 		body,
// 		expected,
// 	)
// }
