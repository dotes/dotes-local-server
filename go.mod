module gitlab.com/dotes/dotes-local-server

go 1.13

require (
	github.com/danwakefield/fnmatch v0.0.0-20160403171240-cbb64ac3d964 // indirect
	github.com/denormal/go-gitignore v0.0.0-20180930084346-ae8ad1d07817
	github.com/google/uuid v1.2.0
	github.com/gorilla/mux v1.8.0
	github.com/mitchellh/go-homedir v1.1.0
	github.com/olekukonko/tablewriter v0.0.4
	github.com/sabhiram/go-gitignore v0.0.0-20201211210132-54b8a0bf510f
	github.com/spf13/afero v1.5.1
	github.com/stretchr/testify v1.7.0
	github.com/tsg/gotpl v0.0.0-20150811134254-a4179fb207b3 // indirect
	gitlab.com/tbhartman/go-matchedfs v1.0.2
	gitlab.com/tbhartman/sectionedfs v0.0.1
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c
)
